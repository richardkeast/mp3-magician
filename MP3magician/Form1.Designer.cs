﻿namespace MP3magician
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.locationText = new System.Windows.Forms.TextBox();
            this.browseLocationButton = new System.Windows.Forms.Button();
            this.includeSubFolders = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.browseDestinationButton = new System.Windows.Forms.Button();
            this.destinationText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.processButton = new System.Windows.Forms.Button();
            this.musicList = new System.Windows.Forms.ListBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(241, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "1. choose music location";
            // 
            // locationText
            // 
            this.locationText.Location = new System.Drawing.Point(38, 56);
            this.locationText.Name = "locationText";
            this.locationText.Size = new System.Drawing.Size(456, 20);
            this.locationText.TabIndex = 1;
            // 
            // browseLocationButton
            // 
            this.browseLocationButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.browseLocationButton.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.browseLocationButton.ForeColor = System.Drawing.Color.White;
            this.browseLocationButton.Location = new System.Drawing.Point(522, 51);
            this.browseLocationButton.Name = "browseLocationButton";
            this.browseLocationButton.Size = new System.Drawing.Size(74, 29);
            this.browseLocationButton.TabIndex = 2;
            this.browseLocationButton.Text = "Browse";
            this.browseLocationButton.UseVisualStyleBackColor = true;
            // 
            // includeSubFolders
            // 
            this.includeSubFolders.AutoSize = true;
            this.includeSubFolders.Checked = true;
            this.includeSubFolders.CheckState = System.Windows.Forms.CheckState.Checked;
            this.includeSubFolders.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.includeSubFolders.ForeColor = System.Drawing.Color.White;
            this.includeSubFolders.Location = new System.Drawing.Point(38, 82);
            this.includeSubFolders.Name = "includeSubFolders";
            this.includeSubFolders.Size = new System.Drawing.Size(138, 21);
            this.includeSubFolders.TabIndex = 3;
            this.includeSubFolders.Text = "include sub folders";
            this.includeSubFolders.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(12, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(270, 30);
            this.label2.TabIndex = 4;
            this.label2.Text = "2. choose music destination";
            // 
            // browseDestinationButton
            // 
            this.browseDestinationButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.browseDestinationButton.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.browseDestinationButton.ForeColor = System.Drawing.Color.White;
            this.browseDestinationButton.Location = new System.Drawing.Point(522, 166);
            this.browseDestinationButton.Name = "browseDestinationButton";
            this.browseDestinationButton.Size = new System.Drawing.Size(74, 29);
            this.browseDestinationButton.TabIndex = 6;
            this.browseDestinationButton.Text = "Browse";
            this.browseDestinationButton.UseVisualStyleBackColor = true;
            // 
            // destinationText
            // 
            this.destinationText.Location = new System.Drawing.Point(38, 171);
            this.destinationText.Name = "destinationText";
            this.destinationText.Size = new System.Drawing.Size(456, 20);
            this.destinationText.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(12, 215);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(156, 30);
            this.label3.TabIndex = 7;
            this.label3.Text = "3. extra options";
            // 
            // processButton
            // 
            this.processButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.processButton.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.processButton.ForeColor = System.Drawing.Color.White;
            this.processButton.Location = new System.Drawing.Point(466, 303);
            this.processButton.Name = "processButton";
            this.processButton.Size = new System.Drawing.Size(130, 29);
            this.processButton.TabIndex = 8;
            this.processButton.Text = "Process Music";
            this.processButton.UseVisualStyleBackColor = true;
            this.processButton.Click += new System.EventHandler(this.processButton_Click);
            // 
            // musicList
            // 
            this.musicList.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.musicList.FormattingEnabled = true;
            this.musicList.ItemHeight = 17;
            this.musicList.Location = new System.Drawing.Point(38, 355);
            this.musicList.Name = "musicList";
            this.musicList.Size = new System.Drawing.Size(558, 140);
            this.musicList.TabIndex = 9;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(38, 501);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(558, 23);
            this.progressBar1.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(625, 531);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.musicList);
            this.Controls.Add(this.processButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.browseDestinationButton);
            this.Controls.Add(this.destinationText);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.includeSubFolders);
            this.Controls.Add(this.browseLocationButton);
            this.Controls.Add(this.locationText);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "MP3 Magician";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox locationText;
        private System.Windows.Forms.Button browseLocationButton;
        private System.Windows.Forms.CheckBox includeSubFolders;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button browseDestinationButton;
        private System.Windows.Forms.TextBox destinationText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button processButton;
        private System.Windows.Forms.ListBox musicList;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}

