﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HundredMilesSoftware.UltraID3Lib;
using System.IO;

namespace MP3magician
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void processButton_Click(object sender, EventArgs e)
        {
            var searchoption = includeSubFolders.Checked ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
            string[] musicFiles = Directory.GetFiles(locationText.Text, "*.mp3", searchoption);
            progressBar1.Maximum = musicFiles.Count();
            progressBar1.Value = 0;

            UltraID3 u = new UltraID3();

            foreach (string musicFile in musicFiles)
            {
                u.Read(musicFile);
                
                musicList.Items.Add(u.Artist + " / " + u.Album + " / " + u.TrackNum + ". " + u.Title);
                musicList.SelectedIndex = musicList.Items.Count - 1;  
                string filename = safeFilename(u.TrackNum + " - " + u.Title);
                string artistFolder = safeFilename(u.Artist);
                string albumFolder = safeFilename(u.Album);
                if (!Directory.Exists(destinationText.Text + artistFolder))
                {
                    Directory.CreateDirectory(destinationText.Text + artistFolder);
                }
                if (!Directory.Exists(destinationText.Text + artistFolder + @"\" + albumFolder))
                {
                    Directory.CreateDirectory(destinationText.Text + artistFolder + @"\" + albumFolder);
                }
                string newFile = destinationText.Text + artistFolder + @"\" + albumFolder + @"\" + filename + ".mp3";
                
                if (!File.Exists(newFile))
                {
                    File.Copy(musicFile, destinationText.Text + artistFolder + @"\" + albumFolder + @"\" + filename + ".mp3");
                }
                progressBar1.Value++;
                Application.DoEvents();
            }
        }

        private string safeFilename(string filename)
        {
            foreach (var c in Path.GetInvalidFileNameChars())
            {
                filename = filename.Replace(c, '-');
            }

            return filename;
        }
    }
}
